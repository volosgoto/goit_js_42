let user = {
    name: "Vova",
    age: 30,
};
// let prop = prompt("Add new prop to object");
// let value = prompt("Add new value");

// user[prop] = "carbonara";
// user[prop] = "carbonara";

// user[prop] = value;

// console.log(user["name"]);
// console.log(user["age"]);

// user.name;
// user.name = "Sara";

// user.status = "Admin";

// console.log(user.name);
// console.log(user);
// ==============================================

// let countries = ["USA", "Britain", "China", "Russia"];
// let vakcines = ["Pfizer", "AstraZeneca", "Coronovac", "Sputnik-V"];
// let bank = {};

// for (let idx in countries) {
//     bank[countries[idx]] = vakcines[idx];
// }

// let len = countries.length;
// for (let i = 0; i < len; i += 1) {
//     bank[countries[i]] = vakcines[i];
// }

// for (let item of countries) {
//     let idx = countries.indexOf(item);
//     // bank[countries[idx]] = vakcines[idx];
//     bank[item] = vakcines[idx];
// }

// console.log(bank);
