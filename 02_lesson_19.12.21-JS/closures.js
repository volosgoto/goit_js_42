// Замыкание - это механизм у функций, когда ей доступны внешние переменые
// Любая ф-ция в JS - это функция высшего порядка (функция первого класса)

// let a = 10;

// const fruits = ["apples", "bannas", "mango", "tomatoes"];

// function getData() {
//     // let a = 20;
//     // console.log(fruits);

//     console.log(a);
// }

// getData();

// {
//     let a = 10;
//     console.log(a);
//     console.log(b);
// }

// {
//     let b = 20;
//     console.log(b);
//     console.log(a);
// }

// ====================================
// function incrementor(n) {

//     return function () {

//         console.log(1 + n);

//     };

// }

// let res = incrementor(10)();
// let res = incrementor(10);
// console.log(res);
// res();

// ===================================

// function incrementor(n) {
//     return function (num) {
//         console.log(num + n);
//     };
// }

// Карирование
// let res = incrementor(10)(50);
// res(20);

// console.log(res);

// =====================================
// function fn() {
//     console.log("fn");
//     return function () {
//         console.log("innter 1");
//         return function () {
//             console.log("inner 2");
//         };
//     };
// }

// fn()()();

// =================================

function farmeWorkManager() {
    let frameWorks = ["Angular", "Vue"];

    return {
        show() {
            console.log(frameWorks);
        },

        add(newFr) {
            frameWorks.push(newFr);
        },
    };
}

let manager = farmeWorkManager();

console.log(manager);

manager.add("Ract");
manager.show();
