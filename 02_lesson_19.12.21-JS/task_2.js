const fruits = ["apples", "bannas", "mango", "tomatoes"];
// fruits.push('pear', 'pizza', 'gamburger');
// console.log(fruits);
// const fruitToFind = 'mango';
// for (let fruit of fruits) {
//     if (fruit === fruitToFind) {
//         console.log('Нашли');
//     } else {
//         continue;
//     }
// }
// function declaration

// function findFruit (){}

// function expretion
// const findFruit = function(){}

// arrow function
// const findFruit = ()=>{}

// const fruitToFind = 'mango';

// function findFruit(name, array) {
// for (let item of array) {
//     if (item === name) {
//         console.log('Нашли');
//         return;
//     }
// }
// }

// findFruit(fruitToFind, fruits)

// const fruitToFind = 'mango';

// function findFruit(fruitToFind, arrayOfFruits) {
//     if (arrayOfFruits.indexOf(fruitToFind) === -1) {
//         console.log('Не нашли');
//         return;
//     }
//     console.log('Нашли');
// }

// findFruit(fruitToFind, fruits)

// function findFruit(fruitToFind, arrayOfFruits) {
//     if (!arrayOfFruits.includes(fruitToFind)) {
//         console.log('Не нашли');
//         return;
//     }
//     console.log('Нашли');
// }

// findFruit(fruitToFind, fruits)

// const fruitToFind = 'pear';

// function findFruit(fruitToFind, arrayOfFruits) {
//     const newFruit = [...arrayOfFruits];

//     if (!arrayOfFruits.includes(fruitToFind)) {
//         newFruit.push(fruitToFind);
//     }
//     return newFruit;
// }

// console.log(findFruit(fruitToFind, fruits));

// const fruitToFind = 'pear';

// function findFruit(fruitToFind, arrayOfFruits) {
//     let newFruit = [];

//     if (!arrayOfFruits.includes(fruitToFind)) {
//         newFruit = [...arrayOfFruits, fruitToFind];
//     }
//     return newFruit;
// }

// console.log(findFruit(fruitToFind, fruits));

const fruitToFind = "pear";

function myPush(arrayOfFruits, ...fruitToFind) {
    let newFruit = [];

    newFruit = [...arrayOfFruits, ...fruitToFind];

    return newFruit;
}

console.log(myPush(fruits, "pear", "pizza", "gamburger"));
