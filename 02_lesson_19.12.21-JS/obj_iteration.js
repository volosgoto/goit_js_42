// let items = [
//     { id: 1, name: "apples", price: 35, amount: 500, category: "fruits" },
//     { id: 2, name: "potato", price: 15, amount: 750, category: "vegatables" },
//     { id: 3, name: "banaba", price: 28, amount: 150, category: "fruits" },
//     { id: 4, name: "tomatoes", price: 20, amount: 350, category: "vegatables" },
// ];

// for (let item of items) {
//     // console.dir(item);
//     // console.log(item.name);
//     console.log(item.amount);
// }

// =======================================

// let cart = [
//     { id: 1, name: "apples", price: 10, qty: 2, category: "fruits" },
//     { id: 2, name: "potato", price: 20, qty: 5, category: "vegatables" },
//     { id: 3, name: "banaba", price: 10, qty: 3, category: "fruits" },
//     { id: 4, name: "tomatoes", price: 20, qty: 10, category: "vegatables" },
// ];

// 35 * 2 = 70
// 15 * 5 = 75

// 70 + 45

// let total = 0;

// for (let cartItem of cart) {
//     let res = cartItem.price * cartItem.qty;
//     total += res;
//     // console.log(res);
// }

// for (let cartItem of cart) {
//     let { price, qty } = cartItem;
//     total += price * qty;
// }

// for (let { price, qty } of cart) {
//     total += price * qty;
// }

// console.log(total);

/////////////////////////////

// let items = [
//     { id: 1, name: "apples", price: 35, amount: 500, category: "fruits" },
//     { id: 2, name: "potato", price: 15, amount: 750, category: "vegatables" },
//     { id: 3, name: "banaba", price: 28, amount: 150, category: "fruits" },
//     { id: 4, name: "tomatoes", price: 20, amount: 350, category: "vegatables" },
// ];

// let shop = {
//     name: "Silpo",
//     adress: "Kyiv",
//     // shopItems: items,
//     // items: items,
//     items,

//     showInfo() {
//         console.log(this.items);
//     },

// addItem(newItem) {
//     // this.items.push(newItem);
//     this.items = [...this.items, newItem];
// },

// addItem(newItem) {
// this.items.push(newItem);
// let id = this.generateID();
// console.log(id);
// this.items = [...this.items, { ...newItem, id: id }];
// },

// generateID() {
//     return Math.random().toString().slice(2);
// },
// };

// function generateID() {
//     return Math.random().toString().slice(2);
// }

// console.log(generateID());

// let kiwi = { name: "kiwi", price: 60, amount: 50, category: "fruits" };

// shop.addItem(kiwi);
// shop.showInfo();

// console.table(shop.items);

let vova = {
    name: "Vova",
    age: 25,
    status: "User",
};

console.log(vova);

let sara = {
    ...vova,
    name: "Sara",
    age: 18,
    id: "qwe",
    adress: "Kyiv",
};

console.log(sara);
