const btnRef = document.querySelector(".js-button");
const pRef = document.querySelector(".js-timer");

btnRef.addEventListener("click", () => {
    let timeLeft = 3;

    const timerID = setInterval(() => {
        timeLeft -= 1;
        if (timeLeft >= 0) {
            pRef.textContent = `Вы покидаете страницу через ${timeLeft}`;
        } else {
            pRef.textContent = "Вы покинули страницу";
            clearInterval(timerID);
        }
    }, 1000);
});

// В lifesever это событие не работает :-(
// window.addEventListener('onbeforeunload', тут пишем колбек)

// setInterval(() => {
//     console.log(new Date());
// }, 1000)
