// class PromotionTimer {
//     constructor(domElem, promotionDate) {
//         this.domElem = domElem;
//         this.promotionDate = promotionDate;
//     }

//     init = () => {
//         let promotionID = setInterval(() => {
//             let dateNow = Date.now();

//             const distance = this.promotionDate - dateNow;

//             const days = Math.floor(distance / (24 * 60 * 60 * 1000));
//             const hours = Math.floor(
//                 (distance % (24 * 60 * 60 * 1000)) / (60 * 60 * 1000)
//             );
//             const mins = Math.floor(
//                 (distance % (60 * 60 * 1000)) / (60 * 1000)
//             );
//             const seconds = Math.floor((distance % (60 * 1000)) / 1000);

//             this.domElem.textContent = `Days: ${days} Hours: ${hours} Minutes: ${mins} Seconds: ${seconds}`;

//             if (distance <= 0) {
//                 clearInterval(promotionID);
//                 this.domElem.textContent = `Акция закончилась`;
//             }
//         }, 1000);
//     };
// }

// =======================================================
class PromotionTimer {
    constructor(domElem, promotionDate) {
        this.domElem = domElem;
        this.promotionDate = promotionDate;
    }

    mutateDate = (distance) => {
        const date = {
            days: Math.floor(distance / (24 * 60 * 60 * 1000)),
            hours: Math.floor(
                (distance % (24 * 60 * 60 * 1000)) / (60 * 60 * 1000)
            ),
            mins: Math.floor((distance % (60 * 60 * 1000)) / (60 * 1000)),
            seconds: Math.floor((distance % (60 * 1000)) / 1000),
        };

        return date;
    };

    renderPromotion = (
        distance,
        promotionID,
        { days, hours, mins, seconds }
    ) => {
        this.domElem.textContent = `Days: ${days} Hours: ${hours} Minutes: ${mins} Seconds: ${seconds}`;

        if (distance <= 0) {
            clearInterval(promotionID);
            this.domElem.textContent = `Акция закончилась`;
        }
    };

    init = () => {
        console.log("Init");
        let promotionID = setInterval(() => {
            let dateNow = Date.now();
            const distance = this.promotionDate - dateNow;

            let calculatedDates = this.mutateDate(distance); // returns {}
            const { days, hours, mins, seconds } = calculatedDates;

            this.renderPromotion(distance, promotionID, {
                days,
                hours,
                mins,
                seconds,
            });
        }, 1000);
    };
}

const pRef = document.querySelector(".js-timer");
const pRef8 = document.querySelector(".js-timer-8");
const promotionDateStValentinesDay = new Date("Feb 14, 2022 00:00:00");
const promotionDateEightOFMarch = new Date("March 08, 2022 00:00:00");

// const pomotionStValentinesDay = new PromotionTimer(
//     pRef,
//     promotionDateStValentinesDay
// ).init();

// const pomotionEightOFMarch = new PromotionTimer(
//     pRef8,
//     promotionDateEightOFMarch
// ).init();

const pomotionStValentinesDay = new PromotionTimer(
    pRef,
    promotionDateStValentinesDay
);

pomotionStValentinesDay.init();
