const pRef = document.querySelector(".js-timer");
const promotionDate = new Date("Feb 13, 2022 11:09:00");
// console.log(promotionDate);

// 'December 17, 1995 03:24:00'

let promotionID = setInterval(() => {
    let dateNow = Date.now();

    const distance = promotionDate - dateNow;

    const days = Math.floor(distance / (24 * 60 * 60 * 1000));
    const hours = Math.floor(
        (distance % (24 * 60 * 60 * 1000)) / (60 * 60 * 1000)
    );
    const mins = Math.floor((distance % (60 * 60 * 1000)) / (60 * 1000));
    const seconds = Math.floor((distance % (60 * 1000)) / 1000);

    pRef.textContent = `Days: ${days} Hours: ${hours} Minutes: ${mins} Seconds: ${seconds}`;

    if (distance <= 0) {
        clearInterval(promotionID);
        pRef.textContent = `Акция закончилась`;
    }
}, 1000);

// ==================================================
