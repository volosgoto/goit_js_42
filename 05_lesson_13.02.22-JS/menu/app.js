const diseases = [
    { name: "Plague", href: "https://en.wikipedia.org/wiki/Plague_(disease)" },
    { name: "Anthrax", href: "https://en.wikipedia.org/wiki/Anthrax" },
    { name: "Ebola", href: "https://en.wikipedia.org/wiki/Ebola" },
    {
        name: "SARS-CoV-2",
        href: "https://en.wikipedia.org/wiki/SARS-CoV-2_Omicron_variant",
    },
];

const btnMenu = document.querySelector("#menu");
const root = document.querySelector("#root");

function createLi({ name, href }) {
    const li = document.createElement("li");
    const a = document.createElement("a");

    a.setAttribute("href", href);
    a.setAttribute("target", "_blank");
    a.textContent = name;
    a.classList.add("item__link");
    li.classList.add("item__list");
    li.append(a);

    return li;
}

const menu = diseases.map((item) => createLi(item));
console.log(menu);

function createUl(menuItems, domElem) {
    const ul = document.createElement("ul");
    ul.classList.add("menu__list");
    ul.append(...menuItems);
    domElem.append(ul);
}

// btnMenu.addEventListener('click', (event) => {
//     console.log(event.target);
//     createUl(menu, root);
//     if (btnMenu.textContent === 'Close Menu') {
//         root.innerHTML = ''
//         btnMenu.textContent = 'Open Menu'
//         return
//     }
//     btnMenu.textContent = 'Close Menu';

// });

// btnMenu.addEventListener('click', (event) => {
//     console.log(event.target);
//     createUl(menu, root);
//     if (event.target.textContent === 'Close Menu') {
//         root.innerHTML = ''
//         event.target.textContent = 'Open Menu'
//         return
//     }
//     event.target.textContent = 'Close Menu';

// });

btnMenu.addEventListener("click", toogleMenu);

function toogleMenu(event) {
    createUl(menu, root);
    if (event.target.textContent === "Close Menu") {
        root.innerHTML = "";
        event.target.textContent = "Open Menu";
        return;
    }
    event.target.textContent = "Close Menu";
}

//console.log(createLi({ name: 'Plague', href: 'https://en.wikipedia.org/wiki/Plague_(disease)' }));
//createLi({ name: 'Plague', href: 'https://en.wikipedia.org/wiki/Plague_(disease)' })
