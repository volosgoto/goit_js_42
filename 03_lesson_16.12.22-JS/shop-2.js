// TODO
// Make class

let items = [
    { id: 1, name: "apples", price: 35, amount: 500, category: "fruits" },
    { id: 2, name: "potato", price: 15, amount: 750, category: "vegatables" },
    { id: 3, name: "banana", price: 28, amount: 150, category: "fruits" },
    { id: 4, name: "tomatoes", price: 20, amount: 350, category: "vegatables" },
];
class Shop {
    constructor(items) {
        this.items = items;
    }

    showProducts() {
        for (let prod of this.products) {
            const { id, name, price, category } = prod;
            console.log(
                `id: ${id}, name: ${name}, price: ${price}, category ${category}`
            );
        }
    }

    addProduct(newprod) {
        let item = { ...newprod, id: this.generateId() };
        this.products = [...this.products, item];
    }

    generateId() {
        let id = Math.random().toString().slice(2);
        return id;
    }

    updateProduct(nameToFind, nameToUpdate) {
        for (let prod of this.products) {
            if (prod.name === nameToFind) {
                prod.name = nameToUpdate;
                return;
            }
            console.log("такого нема");
        }
    }

    removeProduct(nameToRemove) {
        for (let prod of this.products) {
            if (prod.name === nameToRemove) {
                const idx = this.products.indexOf(prod);
                console.log(idx);
                this.products.splice(idx, 1);
                return;
            }
            console.log("такого нема");
        }
    }
}
new Shop();
