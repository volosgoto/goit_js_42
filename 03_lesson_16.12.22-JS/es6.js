// ES6

// class User {
//     constructor(name, salary, position) {
//         this.name = name;
//         this.salary = salary;
//         this.position = position;
//     }

//     getInfo() {
//         console.log(this);
//     }

//     paySalary() {
//         console.log("Pay Salary");
//     }
// }

// let vova = new User("Vova", 1500, "Security");
// let sara = new User("Sara", 500, "accounter");

// console.log(vova);
// console.log(sara);

// // console.log(vova.name);
// // console.log(sara.salary);

// // vova.getInfo();
// // sara.getInfo();

// =====================================
// class User {
//     constructor(name, salary, position) {
//         this.name = name;
//         this.salary = salary;
//         this.position = position;
//     }

//     getInfo() {
//         console.log(this);
//     }

//     paySalary() {
//         console.log("Pay Salary");
//     }
// }

// let vova = new User("Vova", 1500, "Security");
// let sara = new User("Sara", 500, "accounter");

// instanceof

// console.log(vova instanceof User);
// console.log(vova instanceof String);

// class Phone {
//     constructor(title, price) {
//         this.title = title;
//         this.price = price;
//     }

//     getInfo() {
//         console.log(this);
//     }
// }

// let apple = new Phone("Apple", 1500);
// let samsung = new Phone("Samsung", 800);

// console.log(vova instanceof User);
// console.log(vova instanceof Phone);
// console.log(samsung instanceof User);
// console.log(samsung instanceof Phone);

// =============================================

// Getters / setters
// class Phone {
//     constructor(title, price) {
//         this.title = title;
//         this._price = price;
//     }

//     getInfo() {
//         console.log(`Title: ${this.title}, price: ${this.price}`);
//     }

//     get price() {
//         console.log(this._price);
//     }

//     set price(newPrice) {
//         this._price = newPrice;
//     }
// }

// let samsung = new Phone("Samsung", 800);

// // samsung.getInfo();

// //Getter
// // samsung.price;

// //Setter
// samsung.price = 1000;

// console.log(samsung);
// ================================================

// class Phone {
//     constructor(title, price) {
//         this.title = title;
//         this.price = price;
//     }

//     getInfo() {
//         console.log(`Title: ${this.title}, price: ${this.price}`);
//     }

//     getPrice() {
//         console.log(this.price);
//     }

//     setPrice(newPrice) {
//         this.price = newPrice;
//     }
// }

// let samsung = new Phone("Samsung", 800);

// samsung.getPrice();
// samsung.setPrice(1500);
// console.log(samsung);

// ===================================================
// Public field
// class Phone {
//     constructor(title, price) {
//         this.title = title;
//         this.price = price;
//     }

//     // Public field
//     name = "Vova";
//     // Public field
//     serial = "234234";

//     // Public field
//     getInfo = () => {
//         console.log(`Title: ${this.title}, price: ${this.price}`);
//     };

//     // Method
//     getInfo() {
//         console.log(`Title: ${this.title}, price: ${this.price}`);
//     }
// }

// let samsung = new Phone("Samsung", 800);

// console.log(samsung);

// console.log(samsung.name);
// console.log(samsung.serial);

// samsung.getInfo();

// ======================================
// Private fields
// class User {
//     #name;
//     constructor(name, salary, position) {
//         this.#name = name;
//         this.salary = salary;
//         this.position = position;
//     }

//     getInfo() {
//         console.log(this.#name);
//         this.#generateID();
//     }

//     paySalary() {
//         console.log(1000);
//     }

//     #generateID() {
//         let id = Math.random().toString().slice(2);
//         console.log(id);
//         return id;
//     }
// }

// let vova = new User("Vova", 1500, "Security");

// vova.name = null;
// vova.getInfo();
// console.log(vova.name);

// vova.generateID();

// vova.getInfo();

// vova.name = null;

// console.log(vova);

// ====================================
// window.prototype.alert = null;
// alert("Hello");

// window.prototype.console = null;
// console.log("Vova");

// ====================================
let nums = [10, 20, 30, 40, 50];

Array.prototype.pizza = function () {
    console.log("Pizza");
};

nums.pizza();
