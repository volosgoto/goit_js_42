// let items = [
//     { id: 1, name: "apples", price: 35, amount: 500, category: "fruits" },
//     { id: 2, name: "potato", price: 15, amount: 750, category: "vegatables" },
//     { id: 3, name: "banana", price: 28, amount: 150, category: "fruits" },
//     { id: 4, name: "tomatoes", price: 20, amount: 350, category: "vegatables" },
// ];

// // for (let { price} of items) {
// //     console.log(price);
// // };

// const shop = {
//     products: items,
//     name: "fruitsMarket",
//     adress: "odessa",

//     showProducts() {
//         for (let prod of this.products) {
//             const { id, name, price, category } = prod;
//             console.log(
//                 `id: ${id}, name: ${name}, price: ${price}, category ${category}`
//             );
//         }
//     },

//     addProduct(newprod) {
//         this.products.push(newprod);
//     },

//     // updateProduct(nameToFind, nameToUpdate) {
//     //     for (let prod of this.products) {

//     //         if (prod.name === nameToFind) {
//     //             prod.name = nameToUpdate;
//     //             return;
//     //         } console.log('такого нема');
//     //     }
//     // },

//     updateProduct(nameToFind, nameToUpdate) {
//         for (let i = 0; i < this.products.length; i += 1) {
//             if (this.products[i].name === nameToFind) {
//                 this.products[i].name = nameToUpdate;
//                 return;
//             }
//             console.log("такого нема");
//         }
//     },

//     removeProduct() {},
// };

// // shop.addProduct({ id: 5, name: "kiwi", price: 50, amount: 150, category: "fruits" });
// shop.updateProduct("tomatoes", "tomatoes-cherry");

// shop.showProducts();

//============================

let vova = {
    name: "Vova",
    age: 30,
    staus: "Moderator",
};

let sara = {
    ...vova,
    name: "Sara",
    id: "adsfasdfsdfds",
};

console.log(sara);

// =======================================
let items = [
    { id: 1, name: "apples", price: 35, amount: 500, category: "fruits" },
    { id: 2, name: "potato", price: 15, amount: 750, category: "vegatables" },
    { id: 3, name: "banana", price: 28, amount: 150, category: "fruits" },
    { id: 4, name: "tomatoes", price: 20, amount: 350, category: "vegatables" },
];

const shop = {
    products: items,
    name: "fruitsMarket",
    adress: "odessa",

    showProducts() {
        for (let prod of this.products) {
            const { id, name, price, category } = prod;
            console.log(
                `id: ${id}, name: ${name}, price: ${price}, category ${category}`
            );
        }
    },

    addProduct(newprod) {
        let item = { ...newprod, id: this.generateId() };
        this.products = [...this.products, item];
    },
    generateId() {
        let id = Math.random().toString().slice(2);
        return id;
    },

    updateProduct(nameToFind, nameToUpdate) {
        for (let prod of this.products) {
            if (prod.name === nameToFind) {
                prod.name = nameToUpdate;
                return;
            }
            console.log("такого нема");
        }
    },

    removeProduct(nameToRemove) {
        for (let prod of this.products) {
            if (prod.name === nameToRemove) {
                const idx = this.products.indexOf(prod);
                console.log(idx);
                this.products.splice(idx, 1);
                return;
            }
            console.log("такого нема");
        }
    },
};

shop.addProduct({ name: "kiwi", price: 50, amount: 150, category: "fruits" });
//shop.updateProduct('tomatoes', 'tomatoes-cherry');
// shop.removeProduct("tomatoes+");
shop.showProducts();
// shop.generateId();
