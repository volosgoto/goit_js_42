// let vova = {
//     name: 'Vova',
//     salary: 800,
//     position: 'driver'
// }

// let sara = {
//     name: 'Sara',
//     salary: 4500,
//     position: 'Developer'
// }

// let bob = {
//     name: 'Bob',
//     salary: 15000,
//     position: 'CEO'
// }

// ========================

// let vova = {
//     name: "Vova",
//     salary: 800,
//     position: "driver",
// };

// let sara = {
//     ...vova,
//     name: "Sara",
//     position: "Developer",
// };

// ===============================

// ES5
// Constructor, Class
function User(name, salary, position) {
    this.name = name;
    this.salary = salary;
    this.position = position;

    // 1.{}
    // 2 Привязывает this
    // 3. Возвращает этот объект

    // this.paySalary = function () {
    //     console.log("Pay Salary");
    // };
}

User.prototype.getInfo = function () {
    console.log(this);
};

User.prototype.paySalary = function () {
    console.log("Pay Salary");
};

// User("Vova", 1500, "Security");
// new User("Vova", 1500, "Security");

// Instance
// Экземпляр класса, объект, сущьность
let vova = new User("Vova", 1500, "Security");
let sara = new User("Sara", 500, "accounter");

console.log(vova);
console.log(sara);

// console.log(vova.name);
// console.log(sara.salary);

// vova.getInfo();
// sara.getInfo();

// ===========================

// let fruits = ["apples", "bananes", "pear"];
// let nums = [10, 20, 30, 40, 50];

// fruits.push("kiwi");
// fruits.push(1000);

// console.log(fruits);
// console.log(nums);

// String;
// Number;
// Boolean;

// null;
// undefined;
// Symbol;
// BigInt;

// Object;
// User
// Product
// Servert
// Vova
