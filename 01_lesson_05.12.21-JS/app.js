// typeof
// console.log(typeof 'Vova');
// console.log(typeof 100);
// console.log(typeof null);
// console.log(typeof '15px' * 10);
// console.log(typeof NaN);

// const ADMIN_PASSWORD = 'dima';
// let message = '';

// const userInput = prompt('ввeдіть пароль');
// // console.log(userInput);

// if (!userInput) {
//     message = 'Вдмінено користувачем';
// }
// console.log(message);

// 0
// ''
// null
// undefined
// NaN

// [] - true
// {} - true

// if (2 + 2 === 4) {
//     console.log('Hello');
// }

// let msg = 'inDiA';

// msg = msg.toLowerCase();
// msg = msg[0].toUpperCase() + msg.slice(1);

// console.log(msg);

let total = 0;
let input = prompt('Enter number...');

for (let i = 0; input !== null; i += 1) {
    if (isNaN(input)) {
        alert('Вы ввели не число');
        input = prompt('Enter number...');
    } else {
        total += Number(input);
        input = prompt('Enter number...');
    }
}

console.log('Сумма равна = ', total);

// const salaryes = [100, 500, 600, 1000, 1500];
// let totalSalaryes = 0;

// for (let i = 0; i < salaryes.length; i += 1) {
//   totalSalaryes += salaryes[i];
// }
// console.log(totalSalaryes);

const salaryes = [100, 500, 600, 1000, 1500];
let maxSalary = [];

for (let i = 0; i < salaryes.length; i += 1) {
    if (salaryes[i] > 600) {
        maxSalary.push(salaryes[i]);
    }
}
console.log(maxSalary);
