// function StringBuilder(baseValue) {
//     this.value = baseValue;
// }

// StringBuilder.prototype.getValue = function () {
//     return this.value;
// };

// StringBuilder.prototype.padEnd = function (str) {
//     this.value += str;
// };

// StringBuilder.prototype.padStart = function (str) {
//     this.value = str + this.value;
// };

// StringBuilder.prototype.padBoth = function (str) {
//     this.padStart(str);
//     this.padEnd(str);
// };

class StringBuilder {
    constructor(baseValue) {
        this.value = baseValue;
    }

    getValue() {
        return this.value;
    }

    padEnd(str) {
        this.value += str;
        return this;
    }

    padStart(str) {
        this.value = str + this.value;
        return this;
    }

    padBoth(str) {
        this.padStart(str);
        this.padEnd(str);
        return this;
    }
}

// Пиши код выше этой строки
const builder = new StringBuilder(".");
//console.log(builder);
// console.log(builder.getValue()); // '.'
// builder.padStart("^");
// console.log(builder.getValue()); // '^.'
// builder.padEnd("^");
// console.log(builder.getValue()); // '^.^'
// builder.padBoth("=");
// console.log(builder.getValue()); // '=^.^='

console.log(builder.padStart("^").padEnd("^").padBoth("=").getValue());

// const arr = [3, 4, 5, 7, 8, 4];

// const newArr = arr.filter(e => e > 4).reduce((acc, e) => acc + e);
// console.log(newArr);

// const newar1 = newArr.reduce((acc, e) => acc + e);
// console.log(newar1);

// const user = {
//     name: 'Vova',
//     age: 27,

//     getThis() {
//         console.log(this);
//     }
// };
// console.log(user.age);
// user.getThis();
