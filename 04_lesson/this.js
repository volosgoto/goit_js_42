// const vova = {
//     name: 'Vova',
//     age: 27,

//     showInfo() {
//         console.log(this.name, this.age);
//     }
// };

// const sara = {
//     name: 'Sara',
//     age: 21,
// };

// // vova.showInfo.call(sara)

// //карирование
// // vova.showInfo.bind(sara)()

// // const result = vova.showInfo.bind(sara);
// // console.log(result);

// // result();

// const btn = document.querySelector('button');

// // btn.addEventListener('click', vova.showInfo.call(sara))
// // btn.addEventListener('click', onBtnClick)
// btn.addEventListener('click', vova.showInfo.bind(vova))

// function onBtnClick() {
//     console.log('click');
// }

// const cars = {
//     items: [
//         { title: 'BMW', model: 'X6' },
//         { title: 'Audi', model: 'A4' },
//         { title: 'Mersedes', model: 'CLS' },
//     ],

//     getInfo(){
//         for (let item of this.items) {
//             console.log(item.title, item.model);
//         }
//     }
// };
// cars.getInfo()
const germansCars = [
    { title: "BMW", model: "X6" },
    { title: "Audi", model: "A4" },
    { title: "Mersedes", model: "CLS" },
];

const japanisCars = [
    { title: "Toyota", model: "X6" },
    { title: "Hodna", model: "A4" },
    { title: "Mits", model: "CLS" },
];

const francCars = [
    { pizza: "ppp", cola: "sssccc", price: 234 },
    { pizza: "ppp", cola: "sssccc", price: 120 },
    { pizza: "ppp", cola: "ssscccS", price: 150 },
];

// function carInfo(cars) {
//     for (let car of cars) {
//             console.log(car.title, car.model);
//          }
// }

//carInfo(germansCars)
//carInfo(japanisCars)

const info = {
    getInfo() {
        for (let obj of this) {
            let keys = Object.keys(obj);
            let values = Object.values(obj);
            //  console.log(values);
            //  console.log(keys);
            for (let key of keys) {
                console.log(`${key} : ${obj[key]}`);
            }
        }
    },
};
// info.getInfo.bind(germansCars)()

//  info.getInfo.apply(japanisCars)
//info.getInfo.apply(francCars)
